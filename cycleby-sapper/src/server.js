import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import session from 'express-session';
import sessionFileStore from 'session-file-store';
import * as sapper from '@sapper/server';
import { json } from 'body-parser';

const FileStore = sessionFileStore(session);

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

polka() // You can also use Express
  .use(session({
    secret: 'notyoursecret',
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 31536000
    },
    store: new FileStore({
      path: process.env.NOW ? `/tmp/sessions` : `.sessions`
    })
  }))
  .use(json())
	.use(
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		sapper.middleware({
			session: req => ({
				login: req.session && req.session.login
			})
		})
	)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});
