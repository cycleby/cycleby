import ApolloClient from 'apollo-boost';
import fetch from 'node-fetch';

export const client = new ApolloClient({
  uri: process.env.MEETUP_API_URI || 'http://localhost:1337/graphql',
  fetchOptions: {
    credentials: 'include'
  },
  request: async (operation) => {
    // debugger;
    // const token = await AsyncStorage.getItem('token');
    // operation.setContext({
    //   headers: {
    //     authorization: token
    //   }
    // });
  },
  // onError: ({ graphQLErrors, networkError, response }) => {
  //   if (graphQLErrors) {
  //     //sendToLoggingService(graphQLErrors);
  //     console.log("what")
  //   }
  //   if (networkError) {
  //     console.log("what2")
  //     //logoutUser();
  //   }
  //   response.errors = null;
  //   console.log({graphQLErrors, networkError})
  // },
  clientState: {
    defaults: {
      isConnected: true
    },
    resolvers: {
      Mutation: {
        updateNetworkStatus: (_, { isConnected }, { cache }) => {
          cache.writeData({ data: { isConnected }});
          return null;
        }
      }
    }
  },
  cacheRedirects: {
  },
  fetch
});
