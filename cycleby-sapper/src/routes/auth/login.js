import { gql } from "apollo-boost";
import {client} from '../../data';

import { mutate } from "svelte-apollo";

import send from '@polka/send-type';

const LOGIN = gql`
  mutation Login($user: UsersPermissionsLoginInput!) {
    login(input: $user) {
      jwt
      user {
        id
      }
    }
  }
`;

async function login(email, password) {
  try {
    return mutate(client, {
      mutation: LOGIN,
      variables: {
        user: {
          identifier: email,
          password,
          provider: "local"
        }
      }
    });
  } catch (e) {
    // TODO Catch network errors?
    console.log('lololol')
  }
}

export async function post(req, res, next) {
  const { data, errors } = await login(req.body.email, req.body.password);
  if (data) {
    console.log(`User logged in: ${JSON.stringify(data.login.user)}`)
    res.statusCode = 201;
    req.session.login = data.login;
    send(res, 201, data)
  } else if ( errors ) {
    // Gotta handle this
    send(res, 400, errors)
  } else {
    // Really gotta handle this
    send(res, 400)
  }
}